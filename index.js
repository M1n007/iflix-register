const fetch = require('node-fetch');
const uuid = require('uuid/v4')

const getHelloToken = () => new Promise((resolve, reject) => {
    fetch('http://ticketbox.iflix.com/hello', {
        headers:{
            'Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Custom Phone Build/MRA58K) native iflixApp/8 android/3.44.0 build/19703 deviceType/phone',
            'Accept-Language': 'en-US, en;q=0.9',
            'Accept-Parental-Control': '*;TV-Y,TV-Y7,TV-G,TV-PG,G,PG,TV-14,PG-13,TV-MA,R;R',
            'x-iflix-supported': 'state/1',
            'Content-Length': '0',
            'Host': 'ticketbox.iflix.com',
            'Connection': 'close',
        }
    })
    .then(res => res.json())
    .then(res => resolve(res))
    .catch(err => reject(err))
});

const getSubsId = (bundleHelloToken) => new Promise((resolve, reject) => {
    const {policy} = bundleHelloToken;
    const {channel, Authorization, RequestId, RequestDate} = policy.headers;
    fetch(policy.url, {
        headers:{
            'content-type': 'application/json',
            channel,
            Authorization,
            RequestId,
            RequestDate
        }
    })
    .then(res => res.json())
    .then(res => resolve(res))
    .catch(err => reject(err))
});

const activateHelloToken = (bundleHelloToken, subsId) => new Promise((resolve, reject) => {
    const {policy} = bundleHelloToken;
    fetch('http://ticketbox.iflix.com/hello', {
        method: 'POST',
        headers:{
            'x-external-status': '200',
            'x-external-partner': policy.partner,
            'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Custom Phone Build/MRA58K) native iflixApp/8 android/3.44.0 build/19703 deviceType/phone',
            'Accept-Language': 'en-US, en;q=0.9',
            'Accept-Parental-Control': '*;TV-Y,TV-Y7,TV-G,TV-PG,G,PG,TV-14,PG-13,TV-MA,R;R',
            'x-iflix-supported': 'state/1',
            'Content-Type': 'application/json',
            'Content-Length': '70',
            'Host': 'ticketbox.iflix.com',
            'Connection': 'close',
        },
        body:JSON.stringify(subsId)
    })
    .then(res => res.json())
    .then(res => resolve(res))
    .catch(err => reject(err))
});


const register = (bundleHelloToken, resultActivateToken, email, password) => new Promise((resolve, reject) => {
    const dataString = {"email":email,"helloToken":resultActivateToken.bundle,"migrateToken":"","name":"Amin","password":password};
    fetch('https://id.iflix.com/auth/iflix/register', {
        method: 'POST',
        headers:{
            'x-fl': '9a605a63-d15a-4b6f-84af-cb6182052f2d-ixda',
            'x-device-name': 'vbox86p',
            'x-carrier': 'Android/wifi',
            'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Custom Phone Build/MRA58K) native iflixApp/8 android/3.44.0 build/19703 deviceType/phone',
            'Accept-Language': 'en-US, en;q=0.9',
            'Accept-Parental-Control': '*;TV-Y,TV-Y7,TV-G,TV-PG,G,PG,TV-14,PG-13,TV-MA,R;R',
            'x-iflix-supported': 'state/1',
            'X-S': '69863350-dba2-49f4-8b4b-383dc267a849',
            'Authorization': bundleHelloToken.policy.headers.Authorization,
            'Content-Type': 'application/json',
            'Content-Length': '1319',
            'Host': 'id.iflix.com',
            'Connection': 'close',
            'Cookie': `JSESSIONID=5878e9aa0e83e3029764af8e1a44c76e93283391~452A43FFB940BBD8FB960D34A9EF1F45; api-authorization=${bundleHelloToken.policy.headers.Authorization}; _u=${resultActivateToken.bundle}`
        },
        body:JSON.stringify(dataString)
    })
    .then(async res => {
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            response: await res.json()
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const login = (bundleHelloToken, resultActivateToken, email, password) => new Promise((resolve, reject) => {
    const dataString = {"email":email,"helloToken":resultActivateToken.bundle,"password":password};
    fetch('https://id.iflix.com/auth/iflix/login', {
        method: 'POST',
        headers:{
            'x-fl': '9a605a63-d15a-4b6f-84af-cb6182052f2d-ixda',
            'x-device-name': 'vbox86p',
            'x-carrier': 'Android/wifi',
            'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Custom Phone Build/MRA58K) native iflixApp/8 android/3.44.0 build/19703 deviceType/phone',
            'Accept-Language': 'en-US, en;q=0.9',
            'Accept-Parental-Control': '*;TV-Y,TV-Y7,TV-G,TV-PG,G,PG,TV-14,PG-13,TV-MA,R;R',
            'x-iflix-supported': 'state/1',
            'X-S': '69863350-dba2-49f4-8b4b-383dc267a849',
            'Authorization': bundleHelloToken.policy.headers.Authorization,
            'Content-Type': 'application/json',
            'Content-Length': '1319',
            'Host': 'id.iflix.com',
            'Connection': 'close',
            'Cookie': `JSESSIONID=5878e9aa0e83e3029764af8e1a44c76e93283391~452A43FFB940BBD8FB960D34A9EF1F45; api-authorization=${bundleHelloToken.policy.headers.Authorization}; _u=${resultActivateToken.bundle}`
        },
        body:JSON.stringify(dataString)
    })
    .then(async res => {
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            response: await res.json()
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const activateVoucher = (cookie) => new Promise((resolve, reject) => {
    const dataString = `{\"extensions\":{\"persistedQuery\":{\"sha256Hash\":\"88dbf9169a8086c0dcabda082d0dbf69c3ab4791e099236797af98e12d65b976\",\"version\":1}},\"query\":\"        \\n\\nfragment ConversationActionConfirmAndContinue on ConversationActionConfirmAndContinue {\\n  title\\n  text\\n  okButton\\n  cancelButton\\n}\\n\\nfragment KeyValue on KeyValue {\\n  key\\n  value\\n}\\n\\nfragment Layout on Layout {\\n  marginHorizontal\\n  marginTop\\n  marginBottom\\n  paddingHorizontal\\n  paddingTop\\n  paddingBottom\\n}\\n\\nfragment Action on ConversationAction {\\n  action\\n  deeplink\\n  href\\n  ...ConversationActionConfirmAndContinue\\n}\\n\\nfragment ScreenImage on ScreenImage {\\n  url\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment TextStyle on TextStyle {\\n  size\\n  color\\n  linkColor\\n  bold\\n}\\n\\nfragment ScreenText on ScreenText {\\n  textType: type\\n  text\\n  align\\n  style {\\n    ...TextStyle\\n  }\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n  icon {\\n    ...ItemIcon\\n  }\\n}\\n\\nfragment ItemIcon on ItemIcon {\\n  url\\n  size\\n  align\\n}\\n\\nfragment ButtonStyle on ButtonStyle {\\n  leftColor\\n  rightColor\\n  textColor\\n}\\n\\nfragment ScreenButton on ScreenButton {\\n  id\\n  text\\n  buttonType: type\\n  action {\\n    ...Action\\n  }\\n  icon {\\n    ...ItemIcon\\n  }\\n  style {\\n    ...ButtonStyle\\n  }\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n  align\\n  analyticsData {\\n    ...KeyValue\\n  }\\n}\\n\\nfragment ScreenExpandable on ScreenExpandable {\\n  title\\n  text\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment ScreenPhone on ScreenPhone {\\n  id\\n  label\\n  hint\\n  prefixes {\\n    prefix\\n    country\\n  }\\n  value\\n  error\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment ScreenInput on ScreenInput {\\n  id\\n  hint\\n  value\\n  error\\n  inputType: type\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment ScreenSelect on ScreenSelect {\\n  id\\n  hint\\n  optionsType: type\\n  options {\\n    text\\n    value\\n  }\\n  values\\n  error\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment ScreenPricingPlan on ScreenPricingPlan {\\n  name\\n  description\\n  tag {\\n    icon\\n    text\\n    bgColor\\n  }\\n  priceInfo {\\n    currency\\n    price\\n    period\\n    breakdown\\n  }\\n  opened\\n  locked\\n  instruments {\\n    id\\n    icon\\n    title\\n    providers {\\n      icon\\n    }\\n  }\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment ScreenGiabPayment on ScreenGiabPayment {\\n  id\\n  sku\\n  recurring\\n  externalReference\\n}\\n\\nfragment ScreenSeparator on ScreenSeparator {\\n  color\\n  size\\n  margin\\n}\\n\\nfragment ListItem on ListItem {\\n  id\\n  name\\n  icon\\n  analyticsData {\\n    ...KeyValue\\n  }\\n}\\n\\nfragment ScreenList on ScreenList {\\n  headerText\\n  editText\\n  doneText\\n  id\\n  removeId\\n  items {\\n    ...ListItem\\n  }\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n}\\n\\nfragment CsgConfig on CsgConfig {\\n  url: uri\\n  systemId\\n  distributionChannel\\n}\\n\\nfragment ScreenCreditCard on ScreenCreditCard {\\n  id\\n  csgConfig {\\n    ...CsgConfig\\n  }\\n  sessionId\\n  headerText\\n  subheaderText: subHeaderText\\n  nameOnCardLabel\\n  nameOnCardError\\n  cardNumberLabel\\n  cardNumberError\\n  expiryDateLabel\\n  expiryDateError\\n  securityCodeLabel\\n  securityCodeHint\\n  securityCodeError\\n  cardNicknameLabel\\n  cardNicknameHint\\n  cardNicknameError\\n  createButtonLabel\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n  analyticsData {\\n    ...KeyValue\\n  }\\n}\\n\\nfragment ScreenImageButton on ScreenImageButton {\\n  id\\n  url\\n  action {\\n    ...Action\\n  }\\n  layout {\\n    ...Layout\\n  }\\n  bgColor\\n  analyticsData {\\n    ...KeyValue\\n  }\\n}\\n\\nfragment DecoratorTag on DecoratorTag {\\n  icon {\\n    ...ItemIcon\\n  }\\n  text\\n  bgColor\\n}\\n\\nfragment ConversationScreen on ConversationScreen {\\n  id\\n  name\\n  items {\\n    __typename\\n    ...ScreenImage\\n    ...ScreenText\\n    ...ScreenButton\\n    ...ScreenExpandable\\n    ...ScreenPhone\\n    ...ScreenInput\\n    ...ScreenSelect\\n    ...ScreenPricingPlan\\n    ...ScreenGiabPayment\\n    ...ScreenSeparator\\n    ...ScreenCreditCard\\n    ...ScreenList\\n    ...ScreenImageButton\\n    ...DecoratorTag\\n  }\\n  clearStack\\n  bgImage\\n  bgColor\\n  version\\n  toolbarStyle\\n  analyticsData {\\n    ...KeyValue\\n  }\\n}\\n\\n\\n        mutation performConversationAction($conversationId: ID!, $screenId: ID!, $itemId: ID!, $data: [ScreenInputData!], $state: String) {\\n          performConversationAction(\\n            conversationId: $conversationId\\n            screenId: $screenId\\n            itemId: $itemId\\n            data: $data\\n            state: $state\\n          ) {\\n            id\\n            screen {\\n              ...ConversationScreen\\n            }\\n            action {\\n              ...Action\\n            }\\n            state\\n          }\\n        }\",\"variables\":{\"conversationId\":\"payments.VoucherRedemption.${uuid()}\",\"data\":[{\"id\":\"voucher_code\",\"value\":\"DIRUMAHAJA\"}],\"itemId\":\"{\\\"data\\\":{\\\"subject\\\":\\\"VOUCHER_REEDEMPTION_SUBMIT_BTN\\\"},\\\"state\\\":{\\\"region\\\":\\\"ID\\\",\\\"voucherCode\\\":\\\"\\\"}}\",\"screenId\":\"VoucherRedemptionScreen\",\"state\":\"\"}}`;
    fetch('https://gateway.iflix.com/graphql?region=id&language=en-US%2C%20en%3Bq%3D0.9', {
        method: 'POST',
        headers:{
            'User-Agent': 'Dalvik/2.1.0 (Linux; U; Android 6.0; Custom Phone Build/MRA58K) native iflixApp/8 android/3.44.0 build/19703 deviceType/phone',
            'Accept-Parental-Control': '*;TV-Y,TV-Y7,TV-G,TV-PG,G,PG,TV-14,PG-13,TV-MA,R;R',
            'x-iflix-supported': 'state/1',
            'X-S': '69863350-dba2-49f4-8b4b-383dc267a849',
            'Authorization': `Bearer ${cookie[2].split(';')[0].split('=')[1]}`,
            'Content-Type': 'application/json',
            'Host': 'gateway.iflix.com',
            'Cookie': `${cookie[0].split(';')[0]}; ${cookie[1].split(';')[0]}; ${cookie[2].split(';')[0]};`
        },
        body:dataString
    })
    .then(res => res.json())
    .then(res => resolve(res))
    .catch(err => reject(err))
});

(async() => {
    const email = "astajim4@domaindug.com";
    const password = "Coegsekali1"
    const helloToken = await getHelloToken();
    const subsId = await getSubsId(helloToken);
    const resultActivateToken = await activateHelloToken(helloToken, subsId);
    const resultRegister = await register(helloToken, resultActivateToken, email, password);
    if (resultRegister.response.success) {
        console.log(resultRegister)
    }else{
        console.log('ada masalah', resultRegister.response)
    }
})();